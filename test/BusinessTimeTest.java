import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.time.LocalDateTime;

public class BusinessTimeTest {

    private Holiday holiday = new Holiday();

    @BeforeEach
    public void init() {
        holiday.setStart(LocalDateTime.parse("2019-12-24T21:00:00"));
        holiday.setEnd(LocalDateTime.parse("2019-12-25T21:00:00"));
    }

    @ParameterizedTest
    @CsvSource({
        "2019-12-01T00:00:00, 3600, 2019-12-01T01:00:00",
        "2019-12-24T21:00:00, 1, 2019-12-25T21:00:01",
        "2019-12-24T20:30:00, 3600, 2019-12-25T21:30:00",
        "2019-12-25T00:00:00, 1, 2019-12-25T21:00:01",
        "2019-12-25T00:00:00, -1, 2019-12-24T20:59:59"
    })
    public void businessTimeShouldReturnValidDatesAndTime(LocalDateTime date, Integer duration, LocalDateTime expected) {
        Assertions.assertEquals(expected, BusinessTime.addBusinessTime(holiday, date, duration));
    }
}
