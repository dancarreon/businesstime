import java.time.Duration;
import java.time.LocalDateTime;

public class BusinessTime {

    public static LocalDateTime addBusinessTime(Holiday holiday, LocalDateTime time, int duration) {

        long holidayDuration = 0;
        LocalDateTime tTime = time.plusSeconds(duration);

        if (tTime.isAfter(holiday.getStart()) && tTime.isBefore(holiday.getEnd())) {
            if (tTime.getDayOfMonth() == holiday.getEnd().getDayOfMonth()) {
                holidayDuration = Duration.between(time, holiday.getEnd()).toSeconds();
            } else if (tTime.getDayOfMonth() == holiday.getStart().getDayOfMonth() && duration < 0) {
                holidayDuration = Duration.between(holiday.getStart(), time).toSeconds() * -1;
            } else {
                holidayDuration = Duration.between(holiday.getStart(), holiday.getEnd()).toSeconds();
            }
        }

        return time.plusSeconds(holidayDuration).plusSeconds(duration);
    }

    /*
    public static void main(String[] args) {
        Holiday holiday = new Holiday();
        holiday.setStart(LocalDateTime.parse("2019-12-24T21:00:00"));
        holiday.setEnd(LocalDateTime.parse("2019-12-25T21:00:00"));

        print(addBusinessTime(holiday, LocalDateTime.parse("2019-12-01T00:00:00"), 60 * 60)); // returns 2019-12-01T01:00:00
        print(addBusinessTime(holiday, LocalDateTime.parse("2019-12-24T21:00:00"), 1));       // returns 2019-12-25T21:00:01
        print(addBusinessTime(holiday, LocalDateTime.parse("2019-12-24T20:30:00"), 60 * 60)); // returns 2019-12-25T21:30:00
        print(addBusinessTime(holiday, LocalDateTime.parse("2019-12-25T00:00:00"), 1));       // returns 2019-12-25T21:00:01
        print(addBusinessTime(holiday, LocalDateTime.parse("2019-12-25T00:00:00"), -1));      // returns 2019-12-24T20:59:59
    }

    private static void print(LocalDateTime date) {
        System.out.println(date);
    }
    */
}
